# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- Display fingerprint during verification

## [0.4.4] - 2022-03-24
### Changed
- Display claims with errors only in verbose mode 
### Fixed
- Crash when claims has no matched service providers
- Bug where wrong service provider was displayed when verification failed
- Bug where proof URIs showed as "undefined"

## [0.4.3] - 2021-11-17
### Changed
- Updated doipjs dependency

## [0.4.2] - 2021-09-06
### Added
- Drone CI/CD integration

## [0.4.0] - 2021-09-06
### Changed
- Updated doipjs dependency
### Fixed
- Tests

## [0.3.0] - 2021-03-22
### Changed
- Updated doipjs dependency
### Fixed
- Tests

## [0.2.0] - 2020-12-08
Initial release
