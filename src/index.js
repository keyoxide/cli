#!/usr/bin/env node
/*
Copyright (C) 2021 Yarmo Mackenbach

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

If your software can interact with users remotely through a computer network,
you should also make sure that it provides a way for users to get its source.
For example, if your program is a web application, its interface could display
a "Source" link that leads users to an archive of the code. There are many
ways you could offer source, and different solutions will be better for different
programs; see section 13 for the specific requirements.

You should also get your employer (if you work as a programmer) or school,
if any, to sign a "copyright disclaimer" for the program, if necessary. For
more information on this, and how to apply and follow the GNU AGPL, see <https://www.gnu.org/licenses/>.
*/
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import profile from './commands/profile.js'
import verify from './commands/verify.js'
import encrypt from './commands/encrypt.js'

yargs(hideBin(process.argv))
  .scriptName('keyoxide')
  .usage('$0 <cmd> [args]')
  .command(
    ['profile [keyIdentifier]', '$0'],
    'display the keyoxide profile of a key',
    (yargs) => {
      yargs.positional('keyIdentifier', {
        type: 'string',
        default: '',
        describe: 'the identifier of the key, or path to file'
      })
        .example([
          ['keyoxide test@doip.rocks', 'Verify a profile with a HKP key'],
          ['keyoxide profile test@doip.rocks', 'Verify a profile with a WKD key'],
          ['keyoxide profile -v test@doip.rocks', 'Display more information']
        ])
    },
    profile
  )
  .command(
    'verify [path]',
    'verify the validity of a signed document',
    (yargs) => {
      yargs.positional('path', {
        type: 'string',
        default: '',
        describe: 'the path to the signed document'
      })
    },
    verify
  )
  .command(
    'encrypt [keyIdentifier] [message]',
    'encrypt a message',
    (yargs) => {
      yargs.positional('keyIdentifier', {
        type: 'string',
        default: '',
        describe: 'the identifier of the key, or path to file'
      })
        .positional('message', {
          type: 'string',
          default: '',
          describe: 'the message to encrypt'
        })
    },
    encrypt
  )
  .option('verbose', {
    type: 'count',
    description: 'Provide more details in output',
    alias: 'v'
  })
  .option('force-wkd', {
    type: 'boolean',
    description: 'Only search for keys using WKD'
  })
  .option('force-hkp', {
    type: 'boolean',
    description: 'Only search for keys using HKP'
  })
  .option('hkp-server', {
    type: 'string',
    description: 'Specify the HKP server to use'
  })
  .help()
  .parse()
