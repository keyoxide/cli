/*
Copyright (C) 2021 Yarmo Mackenbach

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

If your software can interact with users remotely through a computer network,
you should also make sure that it provides a way for users to get its source.
For example, if your program is a web application, its interface could display
a "Source" link that leads users to an archive of the code. There are many
ways you could offer source, and different solutions will be better for different
programs; see section 13 for the specific requirements.

You should also get your employer (if you work as a programmer) or school,
if any, to sign a "copyright disclaimer" for the program, if necessary. For
more information on this, and how to apply and follow the GNU AGPL, see <https://www.gnu.org/licenses/>.
*/
import fs from 'fs'
import * as doip from 'doipjs'
import chalk from 'chalk'
import ora from 'ora'
import logger from '../logger.js'

export default async (argv) => {
  process.exitCode = 1

  // If no keyIdentifier argument is provided, try and read stdin
  let stdin
  if (!argv.keyIdentifier) {
    try {
      stdin = fs.readFileSync(process.stdin.fd, 'ascii')
    } catch {}
  }

  // If no keyIdentifier argument is provided and stdin is empty,
  // let the user know more input is expected
  if (!argv.keyIdentifier && !stdin) {
    logger.info('Please provide a key URI or path, or pipe key data to stdin\n')
    logger.info('Examples:\n')
    logger.info(
      '- keyoxide 3637202523e7c1309ab79e99ef2dc5827b445f4b\n'
    )
    logger.info(
      '- keyoxide profile 3637202523e7c1309ab79e99ef2dc5827b445f4b\n'
    )
    logger.info('- keyoxide profile test@doip.rocks\n')
    logger.info('- keyoxide profile ./key.asc\n')
    logger.info('- gpg --armor --export test@doip.rocks | keyoxide profile\n')
    return
  }

  // Initialize variables
  let profileData
  let pubKey
  let fetchKeySigSpinner

  try {
    fetchKeySigSpinner = ora('Fetching key or signature').start()

    // If the keyIdentifier is a valid local file
    if (fs.existsSync(argv.keyIdentifier)) {
      // Read the file
      const fileContent = fs.readFileSync(argv.keyIdentifier, 'ascii')

      // Check if the file is an OpenPGP key or a signature profile
      if (fileContent.includes('BEGIN PGP SIGNED MESSAGE')) {
        // Process the signature profile
        profileData = await doip.signatures.process(fileContent)
        pubKey = profileData.key
      } else {
        // Read the key file
        pubKey = await doip.keys.fetchPlaintext(fileContent)
      }
    } else if (stdin) {
      // Check if stdin is an OpenPGP key or a signature profile
      if (stdin.includes('BEGIN PGP SIGNED MESSAGE')) {
        // Process the signature profile
        profileData = await doip.signatures.process(stdin)
        pubKey = profileData.key
      } else {
        // Read the key file
        pubKey = await doip.keys.fetchPlaintext(stdin)
      }
    } else {
      if (argv.forceWkd) {
        pubKey = await doip.keys.fetchURI(`wkd:${argv.keyIdentifier}`)
      } else if (argv.forceHkp && argv.hkpServer) {
        pubKey = await doip.keys.fetchURI(`hkp:${argv.hkpServer}:${argv.keyIdentifier}`)
      } else if (argv.forceHkp) {
        pubKey = await doip.keys.fetchURI(`hkp:${argv.keyIdentifier}`)
      } else {
        pubKey = await doip.keys.fetch(argv.keyIdentifier)
      }
    }
    fetchKeySigSpinner.stop()
  } catch (e) {
    fetchKeySigSpinner.stop()
    logger.error(`Error when fetching the profile data: ${e.message}\n`)
    return
  }

  // If the source is a key, process it
  if (!profileData) {
    let processKeySpinner
    try {
      processKeySpinner = ora('Processing key').start()
      profileData = await doip.keys.process(pubKey)
      processKeySpinner.stop()
    } catch (e) {
      processKeySpinner.stop()
      logger.error(`Error when processing the key: ${e}\n`)
      return
    }
  }

  // Log the PGP key fingerprint
  logger.log(`PGP key fingerprint: ${profileData.fingerprint}\n`)

  // Prepare the claim verification process
  const verClaimsSpinner = ora('Verifying claims').start()
  const promises = []
  const doipOpts = {
    proxy: {
      hostname: 'proxy.keyoxide.org',
      policy: 'adaptive'
    }
  }

  // Generate a long list with each claim for each user
  profileData.users.forEach(user => {
    user.claims.forEach(claim => {
      claim.match()
      promises.push(claim.verify(doipOpts))
    })
  })

  // Wait for all claims to be verified
  await Promise.all(promises)
    .then(values => {
      verClaimsSpinner.stop()
    })
    .catch(e => {
      logger.error(`Error when verifying the claims: ${e}\n`)
    })

  logger.log('Verification results:\n')

  // For each userID found in the key
  for (let iUser = 0; iUser < profileData.users.length; iUser++) {
    let index = iUser

    // Make sure to handle the primary user first, then the others
    if (index === 0) {
      index = profileData.primaryUserIndex
    } else if (index <= profileData.primaryUserIndex) {
      index--
    }

    // Log the userID
    logger.log(`${chalk.bold(profileData.users[index].userData.id)}\n`)

    // Notify when no claims where found
    if (profileData.users[index].claims.length === 0) {
      logger.log('    No claims for this identity\n')
    }

    // Filter the claims that did not match any service providers
    const claimsNoMatchedSP = profileData.users[index].claims.filter(x => x.matches.length === 0)
    const claimsUnordered = profileData.users[index].claims.filter(x => x.matches.length > 0)

    // Sort the remaining claims
    const claimsOrdered = claimsUnordered.sort((a, b) =>
      a.matches[0].serviceprovider.name >
      b.matches[0].serviceprovider.name
        ? 1
        : a.matches[0].serviceprovider.name ===
          b.matches[0].serviceprovider.name
          ? a.matches[0].profile.display >
          b.matches[0].profile.display
              ? 1
              : -1
          : -1
    )

    // Log valid claims
    claimsOrdered.forEach((claim, iClaim) => {
      const prefix = claim.verification.result ? chalk.green('✓') : chalk.red('x')
      const serviceproviderName =
        !claim.verification.result && !claim.matches[0].serviceprovider.isAmbiguous
          ? '?'
          : claim.matches[0].serviceprovider.name

      // Log basic information about the claim
      logger.log(
        `  ${prefix} ${claim.matches[0].profile.display} (${serviceproviderName})\n`,
        argv
      )

      // Log optional detailed information about the claim
      if (argv.verbose > 0) {
        logger.log(
          `    Profile URL: ${claim.matches[0].profile.uri}\n`
        )
        logger.log(
          `    Proof URL: ${claim.matches[0].proof.uri || 'no URL available'}\n`
        )
      }
    })

    // Log claims that did not match any service providers
    if (argv.verbose > 0) {
      claimsNoMatchedSP.forEach((claim, iClaim) => {
        logger.log(
          `  ${chalk.yellow('!')} ${claim.uri}\n`
        )
        logger.log(
          '    Error: no matched service providers\n'
        )
      })
    }
  }

  process.exitCode = 0
}
