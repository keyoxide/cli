PREFIX ?= /usr/local
BINDIR ?= ${PREFIX}/bin
DATADIR ?= ${PREFIX}/share

BUILD_DIR := ./build
TARGET_EXEC := keyoxide

$(BUILD_DIR)/$(TARGET_EXEC):
	@npm run build

.PHONY: install
install: $(BUILD_DIR)/$(TARGET_EXEC)
	@install -vD -m755 $(BUILD_DIR)/$(TARGET_EXEC) ${DESTDIR}${BINDIR}/keyoxide

.PHONY: clean
clean:
	@rm -r $(BUILD_DIR)