import { spawn } from 'child_process'
import chai from 'chai'
import stripAnsi from 'strip-ansi'

const expectedStdout = [
  'PGP key fingerprint: 3637202523e7c1309ab79e99ef2dc5827b445f4b',
  'Verification results:',
  'Yarmo Mackenbach (material for test frameworks) <test@doip.rocks>',
  '  ✓ doip.rocks (dns)',
]

describe('profile', () => {
  it('should accept a valid email address', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'profile', 'test@doip.rocks'])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should accept a valid HKP email address', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'profile', '--force-hkp', 'test@doip.rocks'])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should accept a valid HKP email address + keyserver URI', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', [
        './src',
        'profile',
        '--force-hkp',
        'keyserver.ubuntu.com:test@doip.rocks',
      ])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should accept a valid --hkp-server flag', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', [
        './src',
        'profile',
        '--force-hkp',
        '--hkp-server',
        'keyserver.ubuntu.com',
        'test@doip.rocks',
      ])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should accept a valid HKP fingerprint', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', [
        './src',
        'profile',
        '--force-hkp',
        '3637202523e7c1309ab79e99ef2dc5827b445f4b',
      ])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should accept a valid WKD email address', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'profile', '--force-wkd', 'test@doip.rocks'])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should accept a path to a valid key file', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'profile', './test/test@doip.rocks.asc'])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should accept stdin (public key)', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'profile'], {
        stdio: [spawn('cat', ['./test/test@doip.rocks.asc']).stdout, 'pipe', 'pipe']
      })
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should accept a path to a signature profile', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'profile', './test/signature-profile.asc'])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should accept stdin (signature profile)', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'profile'], {
        stdio: [spawn('cat', ['./test/signature-profile.asc']).stdout, 'pipe', 'pipe']
      })
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should reject a non-existing HKP URI', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', [
        './src',
        'verify',
        'hkp:not-a-test@doip.rocks',
      ])
      proc.on('close', (code) => {
        if (code != 1) {
          reject(new Error('Exit code was not 1'))
        }
        resolve()
      })
    })
  })
  it('should reject a non-existing WKD URI', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', [
        './src',
        'verify',
        'wkd:not-a-test@doip.rocks',
      ])
      proc.on('close', (code) => {
        if (code != 1) {
          reject(new Error('Exit code was not 1'))
        }
        resolve()
      })
    })
  })
  it('should reject an invalid URI', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'profile', 'inv:test@doip.rocks'])
      proc.on('close', (code) => {
        if (code != 1) {
          reject(new Error('Exit code was not 1'))
        }
        resolve()
      })
    })
  })
  it('should reject an invalid signature profile', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'profile', './test/signature-profile.invalid.asc'])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 1) {
          reject(new Error('Exit code was not 1'))
        }
        resolve()
      })
    })
  }).timeout('12s')
})
